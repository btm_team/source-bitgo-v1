'use strict';

Promise = require('bluebird');
const crypto = require('crypto');
const SourceAccessError = require('./errors/sourceAccessError');
const SourceError = require('./errors/sourceError');
const BitGo = require('bitgo');
const utils = require('./utils');
const sourceName = 'source-bitgo-v1';

class SourceBitgo {
    constructor(authorizeData = {}) {
        this.walletId = authorizeData.walletId;
        this.password = authorizeData.password;
        try {
            this.bitgo = new BitGo.BitGo({ env: 'prod', accessToken: authorizeData.accessToken });
        }
        catch(e) {
            this._error(e);
        }
        this.support = {
            crypto: ['btc']
        };
        
        this.session = false;
        }
    
    _openSession() {
            if (this.session) return Promise.resolve();

            return new Promise((resolve, reject) => {
                this.bitgo.session({}, (err, res) => {
                    if (err) return reject(err);
                    this.session = true;
                    resolve();
                });
            });
        }

    _getWallet() {
        return this._openSession()
            .then(() => {
                return new Promise((resolve, reject) => {
                    this.bitgo.wallets().get({ id: this.walletId}, (err, wallet) => {
                        if (err) return reject(err);
                        resolve(wallet);
                    });
                });
            });
    }

    deposit() {
        return Promise.resolve(this.walletId);
    }
    
    balance() {
        return this._listWalletUnspents()
            .then(unspents => {
                let balance = 0;
                unspents.forEach(unspent => {
                    balance += unspent.value;
                });
                return balance / 1e8;
            })
            .catch(err => { throw this._error(err); });
    }

    _listWalletUnspents() {
        return this._getWallet()
            .then(wallet => {
                return new Promise((resolve, reject) => {
                    wallet.unspents({limit: 250}, (err, unspents) => {
                        if (err) return reject(err);
                        resolve(unspents);
                    });                
                });
            });
    }

    send(currency, address, amount, fee) {
        if (this.support.crypto.indexOf(currency.toLowerCase()) === -1) 
            return Promise.reject(new SourceSendError(`Source not support currency "${currency}"`), 'send');

        const id = crypto.randomBytes(20).toString('hex');
        return this._getWallet()
            .then(wallet => {
                return new Promise((resolve, reject) => {
                    const sendObject = {
                        sequenceId: id,
                        address: address, 
                        amount: parseInt(amount * 1e8), 
                        walletPassphrase: this.password,
                        minConfirms: 0
                    };
                    if (fee) sendObject.fee = parseInt(fee * 1e8);
                    wallet.sendCoins(sendObject, (err, result) => {
                        if (err) return reject(err);
                        return resolve({
                            id: id,
                            status: 'finished',
                            txId: result.hash
                        });
                    });
                });
            })
            .catch(err => {
                throw this._error(err, 'send');
            });
    }

    sendMany(currency, recipients, fee) {
        if (this.support.crypto.indexOf(currency.toLowerCase()) === -1) 
            return Promise.reject(new SourceSendError(`Source not support currency "${currency}"`), 'send');

        const id = crypto.randomBytes(20).toString('hex');

        recipients = recipients.map(recipient => {
            recipient.amount = parseInt(recipient.amount * 1e8);
            return recipient;
        });

        return this._getWallet()
            .then(wallet => {
                return new Promise((resolve, reject) => {
                    const sendObject = {
                        sequenceId: id,
                        recipients: recipients,
                        walletPassphrase: this.password,
                        minConfirms: 0
                    };
                    if (fee) sendObject.fee = parseInt(fee * 1e8);
                    wallet.sendMany(sendObject, (err, result) => {
                        if (err) return reject(err);
                        return resolve({
                            id: id,
                            status: 'finished',
                            txId: result.hash
                        });
                    });
                });
            })
            .catch(err => {
                throw this._error(err, 'sendMany');
            });
    }

    _error(err, method) {
        const message = err.message.toLowerCase();
        if (message.indexOf('unauthorized') !== -1) throw new SourceAccessError('bitgo', method, err.message, 'ERR_SOURCE_ACCESS_UNAUTHORIZED');
        //if (message.indexOf('attempt to use ip-restricted token from an unauthorized ip address') !== -1) return new SourceAccessError('bitstamp', method, err.message, 'ERR_SOURCE_ACCESS_IP_ERROR');
        if (message.indexOf('insufficient funds') !== -1) return new SourceError(sourceName, method, err.message, 'ERR_SOURCE_INSUFFICIENT_FUNDS_TO_SEND');
        if (message.indexOf('no unspents available on wallet') !== -1) return new SourceError(sourceName, method, err.message, 'ERR_SOURCE_NO_UNSPENTS_AVAILABLE');
        if (message.indexOf('cannot spend unconfirmed external input') !== -1) return new SourceError(sourceName, method, err.message, 'ERR_SOURCE_UNCONFIRMED_INPUT');
        if (message.indexOf('output value is less than dust threshold') !== -1) return new SourceError(sourceName, method, err.message, 'ERR_SOURCE_LIMIT_SEND');
        if (message.indexOf('fee rate too low') !== -1) return new SourceError(sourceName, method, err.message, 'ERR_SOURCE_LIMIT_FEE');
        return new SourceError(sourceName, method, err.message, 'ERR_SOURCE_ERROR');
    }
};

module.exports = {
    type: 'wallet',
    source: SourceBitgo,
    errors: { SourceError, SourceAccessError }
};