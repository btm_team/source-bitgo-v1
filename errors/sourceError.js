'use strict';

class SourceError extends Error {
    constructor(sourceName, methodName, message, code) {
        super(message);
        this.name = 'SourceError';
        this.sourceName = sourceName;
        this.methodName = methodName;
        this.code = code;

        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, SourceError);
        } else {
            this.stack = (new Error()).stack;
        }
    }
}

module.exports = SourceError;